# Rock Paper Scissors Game

This is a simple implementation of the Rock Paper Scissors game in PHP. In this game, Player 1 always chooses "rock", and Player 2 makes a random choice between "rock", "paper", and "scissors". The winner of each round is determined based on the rules: rock beats scissors, paper beats rock, and scissors beats paper. If both players make the same choice, it's a draw.

## How to Play

To play the game, follow these steps:

1. Clone or download the repository to your local machine.
2. Open a terminal and navigate to the directory containing the game files.
3. Run the `RockPaperScissors.php` file using PHP to start the game:

 `php RockPaperScissors.php`

4. The game will automatically play 100 rounds. After completing the rounds, it will display the final statistics, including the number of wins for each player and the number of draws.

## How to Test

To run PHPUnit tests for the Rock Paper Scissors game, follow these steps:

1. Ensure you have PHPUnit installed globally or as a project dependency. If not, you can install it using Composer:

   `composer require --dev phpunit/phpunit`
   
2. Open a terminal and navigate to the directory containing the game files.
3. Run the PHPUnit tests using the following command:

    `./vendor/bin/phpunit RockPaperScissorsTest.php`

This command will execute all the test cases defined in the `RockPaperScissorsTest.php` file and display the test results.


