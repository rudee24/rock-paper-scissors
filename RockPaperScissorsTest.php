<?php

require_once 'RockPaperScissors.php';

use PHPUnit\Framework\TestCase;

class RockPaperScissorsTest extends TestCase {
    public function testGame() {
        $game = new RockPaperScissors();
        $game->playGame();

        $player1Wins = $game->getPlayer1Wins();
        $player2Wins = $game->getPlayer2Wins();
        $draws = $game->getDraws();

        // Ensure the total number of rounds played is 100
        $totalRounds = $player1Wins + $player2Wins + $draws;
        $this->assertEquals(100, $totalRounds);

        // Test getWinner() method directly
        $reflectionClass = new ReflectionClass('RockPaperScissors');
        $method = $reflectionClass->getMethod('getWinner');
        $method->setAccessible(true);

        // Test for all possible combinations of player1 and player2 choices
        foreach (['rock', 'paper', 'scissors'] as $player1Choice) {
            foreach (['rock', 'paper', 'scissors'] as $player2Choice) {
                $result = $method->invokeArgs($game, [$player1Choice, $player2Choice]);
                if ($player1Choice == $player2Choice) {
                    $this->assertEquals('draw', $result);
                } elseif (($player1Choice == 'rock' && $player2Choice == 'scissors') ||
                          ($player1Choice == 'paper' && $player2Choice == 'rock') ||
                          ($player1Choice == 'scissors' && $player2Choice == 'paper')) {
                    $this->assertEquals('player1', $result);
                } else {
                    $this->assertEquals('player2', $result);
                }
            }
        }
    }
}

?>
