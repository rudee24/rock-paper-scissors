<?php

class RockPaperScissors {
    private $player1Choice = 'rock';
    private $player2Choices = ['rock', 'paper', 'scissors'];
    private $rounds = 100;
    private $player1Wins = 0;
    private $player2Wins = 0;
    private $draws = 0;

    public function playGame() {
        for ($i = 1; $i <= $this->rounds; $i++) {
            $player2Choice = $this->player2Choices[array_rand($this->player2Choices)];
            $result = $this->getWinner($this->player1Choice, $player2Choice);
            $this->updateStatistics($result);
        }
        $this->displayStatistics(); // Remove this line to avoid echoing in tests
    }

    public function getPlayer1Wins() {
        return $this->player1Wins;
    }

    public function getPlayer2Wins() {
        return $this->player2Wins;
    }

    public function getDraws() {
        return $this->draws;
    }

    private function getWinner($player1Choice, $player2Choice) {
        if ($player1Choice == $player2Choice) {
            return 'draw';
        } elseif (($player1Choice == 'rock' && $player2Choice == 'scissors') ||
                  ($player1Choice == 'paper' && $player2Choice == 'rock') ||
                  ($player1Choice == 'scissors' && $player2Choice == 'paper')) {
            return 'player1';
        } else {
            return 'player2';
        }
    }

    private function updateStatistics($result) {
        switch ($result) {
            case 'player1':
                $this->player1Wins++;
                break;
            case 'player2':
                $this->player2Wins++;
                break;
            case 'draw':
                $this->draws++;
                break;
        }
    }

    private function displayStatistics() {
        echo "Player 1 wins: " . $this->player1Wins . PHP_EOL;
        echo "Player 2 wins: " . $this->player2Wins . PHP_EOL;
        echo "Draws: " . $this->draws . PHP_EOL;
    }
}

$game = new RockPaperScissors();
$game->playGame();

?>
